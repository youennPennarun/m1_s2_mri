package mri.socket.tcp.charset;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServeurTCP {
	public static void main(String[] args) {
		if (args.length < 1) {
			System.err.println("Missing args");
			System.exit(-1);
		}
		try (ServerSocket server = new ServerSocket(9999)) {
			boolean running = true;
			System.out.println("Server up and running");
			while (running) {
				Socket client = server.accept();
				System.out.println("New client");
				traiterSocketCliente(client, args[0]);
				System.out.println("Done...");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void traiterSocketCliente(Socket socketVersUnClient,
			String charset) {
		PrintWriter writer = null;
		BufferedReader reader = null;
		try {
			writer = creerPrinter(socketVersUnClient, charset);
			reader = creerReader(socketVersUnClient, charset);
			String name = avoirNom(reader);

			String msg = recevoirMessage(reader);
			while (msg != null) {
				msg = name + "> " + msg;
				System.out.println(msg);
				envoyerMessage(writer, msg);
				msg = recevoirMessage(reader);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null)
				writer.close();
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static BufferedReader creerReader(Socket socketVersUnClient,
			String charset) throws IOException {
		return new BufferedReader(new InputStreamReader(
				socketVersUnClient.getInputStream(), charset));
	}

	public static PrintWriter creerPrinter(Socket socketVersUnClient,
			String charset) throws IOException {
		return new PrintWriter(new OutputStreamWriter(
				socketVersUnClient.getOutputStream(), charset));
	}

	public static String recevoirMessage(BufferedReader reader)
			throws IOException {
		return reader.readLine();
	}

	public static String avoirNom(BufferedReader reader) throws IOException {
		String[] lines = recevoirMessage(reader).split("Name: ");
		if (lines.length < 2)
			return "anonymous";
		else
			return lines[1];
	}

	public static void envoyerMessage(PrintWriter printer, String message)
			throws IOException {
		printer.println(message);
		printer.flush();
	}
}