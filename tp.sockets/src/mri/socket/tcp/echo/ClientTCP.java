package mri.socket.tcp.echo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientTCP {
	public static void main(String[] args) {
		PrintWriter printer = null;
		BufferedReader reader = null;
		try (Socket client = new Socket("localhost", 9999)) {
			printer = creerPrinter(client);
			reader = creerReader(client);
			String input = "";
			boolean running = true;
			while (running) {
				input = lireMessageAuClavier();
				if (!input.equals("fin")) {
					envoyerMessage(printer, input);
					System.out.println("server: " + recevoirMessage(reader));
				} else {
					running = false;
				}
			}
			System.out.println("ended...");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (printer != null)
				printer.close();
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	// Lire un message au clavier
	// envoyer le message au serveur
	// recevoir et afficher la réponse du serveur
	public static String lireMessageAuClavier() throws IOException {
		String input = null;
		System.out.print(":>");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		input = in.readLine();
		return input;
	}

	public static BufferedReader creerReader(Socket socketVersUnClient)
			throws IOException {
		return new BufferedReader(new InputStreamReader(
				socketVersUnClient.getInputStream()));
	}

	public static PrintWriter creerPrinter(Socket socketVersUnClient)
			throws IOException {
		return new PrintWriter(socketVersUnClient.getOutputStream());
	}

	public static String recevoirMessage(BufferedReader reader)
			throws IOException {
		return reader.readLine();
	}

	public static void envoyerMessage(PrintWriter printer, String message)
			throws IOException {
		printer.println(message);
		printer.flush();
	}

}
