package mri.socket.udp.multicast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ChatMulticast {

	public static boolean running;

	static Logger log = Logger.getLogger(ChatMulticast.class.getName());

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String ip;
		Scanner in = new Scanner(System.in);
		if (args.length < 1) {
			System.out.print("IP: ");
			ip = in.nextLine();
		} else {
			ip = args[0];
		}
		int port = 9999;
		InetAddress groupeIP = null;
		MulticastSocket socket = null;
		try {
			groupeIP = InetAddress.getByName(ip);
			socket = new MulticastSocket(port);
			socket.joinGroup(groupeIP);

		} catch (UnknownHostException e1) {
			e1.printStackTrace();
			System.exit(-1);
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}

		System.out.print("name: ");
		String name = in.nextLine();
		System.out.println();
		log.info("Name set to " + name);
		running = true;
		createReceiverThread(socket);
		while (running) {
			String input = (name + ">" + in.nextLine());
			byte[] contenuMessage = input.getBytes();
			log.log(Level.FINE, "Sending " + input);
			DatagramPacket message;
			message = new DatagramPacket(contenuMessage, contenuMessage.length,
					groupeIP, port);
			try {
				socket.send(message);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		in.close();
		socket.close();
	}

	public static void createReceiverThread(final MulticastSocket socket) {
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				log.info("Waiting for messages");
				try {
					while (ChatMulticast.running)
						System.out.println(read(socket));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		t.setDaemon(true);
		t.start();
	}

	public static String read(MulticastSocket socket) throws IOException {
		byte[] contenuMessage = new byte[1024];
		DatagramPacket message = new DatagramPacket(contenuMessage,
				contenuMessage.length);
		socket.receive(message);
		return new String(contenuMessage);
	}

}
