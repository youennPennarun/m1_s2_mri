package fr.istic.date.route;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

public class EnvoyerDate {

  private static final String EXCHANGE_NAME = "date_route";

  public static void main(String[] argv) throws Exception {
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();
    channel.exchangeDeclare(EXCHANGE_NAME, "direct", true);

    String message = getDate();
    String messageGMT = getDateGMT();
    int i = 0;
    while(i<50)
    {
    	TimeUnit.SECONDS.sleep(1);
        channel.basicPublish(EXCHANGE_NAME, "loc", null, message.getBytes("UTF-8"));
        channel.basicPublish(EXCHANGE_NAME, "gmt", null, messageGMT.getBytes("UTF-8"));
        System.out.println(" [x] Sent '" + message + "'");
        System.out.println(" [x] Sent gmt'" + messageGMT + "'");
    	i++;
    	message = getDate();
    	messageGMT = getDateGMT();
    }


    channel.close();
    connection.close();
  }

  private static String getDate(){
   return (new Date()).toString();

  }
  

  private static String getDateGMT(){
   return (new Date()).toGMTString();

  }

  private static String joinStrings(String[] strings, String delimiter) {
    int length = strings.length;
    if (length == 0) return "";
    StringBuilder words = new StringBuilder(strings[0]);
    for (int i = 1; i < length; i++) {
        words.append(delimiter).append(strings[i]);
    }
    return words.toString();
  }
}