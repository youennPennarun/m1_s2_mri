package fr.istic.chat;

import java.io.BufferedReader;
import java.io.InputStreamReader;


import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

public class EnvoyerChat {

  private static final String EXCHANGE_NAME = "chat";
 
  public static void main(String[] argv) throws Exception {
	  String topicName;
	  String guestName;
	  if(argv.length==2)
	  {
		  topicName = argv[0];
		  guestName = argv[1];
	  }else
	  {
		  topicName = "chat.mry";
		  guestName = "anonymous";
	  }
	  System.out.println("Username:"+guestName);
	  Afficheur afficheur = new Afficheur(topicName);
	  afficheur.run();
	  
    ConnectionFactory factory = new ConnectionFactory();
    factory.setHost("localhost");
    Connection connection = factory.newConnection();
    Channel channel = connection.createChannel();
    channel.exchangeDeclare(EXCHANGE_NAME, "topic", true);
    int i = 0;
    BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
    while(i<5000)
    {
        String message = topicName+'#'+guestName+'>'+bufferRead.readLine();
        channel.basicPublish(EXCHANGE_NAME, topicName, null, message.getBytes("UTF-8"));
        //System.out.println(" [x] Sent '" + message + "'");
    	i++;
    }
    channel.close();
    connection.close();
  }

}