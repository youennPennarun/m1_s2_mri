package mri.jersey.client;

import java.net.URI;
import java.util.Date;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import mri.jersey.message.Message;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class TestClient {

  public static void main(String[] args) {
    ClientConfig config = new DefaultClientConfig();
    Client client = Client.create(config);

    URI serviceURI = UriBuilder
        .fromUri("http://localhost:8080/mri.jersey/rest").build();
    WebResource service = client.resource(serviceURI);

    // Exemple cr�ation d'un message :
    Message m = new Message();
    m.setContent("Client test");

    m = (Message) service.path("messages").accept(MediaType.APPLICATION_JSON)
        .post(Message.class, m);
    
    
    int mid=0;
    
    System.out.println("Original list ="+service.path("messages/").accept(MediaType.APPLICATION_JSON).get(String.class));
    System.out.println("Delete Message with id: "+mid);    
    service.path("messages/"+mid).accept(MediaType.APPLICATION_JSON).delete();
    System.out.println("New list ="+service.path("messages").accept(MediaType.APPLICATION_JSON).get(String.class));    
    Message mpost = new Message(); 
    mpost.setContent("Add message by post");
    mpost.setId((long) 4);
    mpost.setDate(new Date().toString());
    System.out.println("Add message via POST: content="+mpost.getContent()+",id="+mpost.getId()+",date="+mpost.getDate());
    service.path("messages").accept(MediaType.APPLICATION_JSON)
    .post(Message.class, mpost);
    System.out.println("List after post ="+service.path("messages").accept(MediaType.APPLICATION_JSON).get(String.class));    
    mid=2;
    System.out.println("Print every message with id superior or equal to "+ mid);
    System.out.println(service.path("messages/after/"+mid).accept(MediaType.APPLICATION_JSON).get(String.class));
    mid=4;
    System.out.println("Print only the message with id equal to "+ mid);
    System.out.println(service.path("messages/"+mid).accept(MediaType.APPLICATION_JSON).get(String.class));
    // A vous de continuer le code ...

  }
}