package mri.jersey.message;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.xml.bind.JAXBElement;

/*chaque resource a un url*/
/*get path*/
/*add path*/
/* annotation */
@Path("/")
public class MessageResource {

	/* annotations */
	@GET
	@Path("/messages")
	public List<Message> getMessages() {
		System.out.println("retourne la liste des messages");
		return MessageList.getInstance().getMessages();
	}

	/* annotations */
	@GET
	@Path("messages/after/{id}")
	public List<Message> getMessagesAfter(@PathParam("id") Long id) {
		// System.out.println("retourne la liste des messages apr�s " + id);
		return MessageList.getInstance().getMessagesAfter(id);
	}

	@POST
	@Path("/messages")
	public Message create(JAXBElement<Message> message) {
		System.out.println("ajoute le message et retourne le message complet");
		// TODO : A COMPLETER
		return MessageList.getInstance().createMessage(message.getValue());
	}

	/* annotations */
	@GET
	@Path("/messages/{id}")
	public Message getMessage(@PathParam("id") Long id) {
		System.out.println("retourne le message d'id = " + id);
		// TODO : A COMPLETER
		return MessageList.getInstance().getMessage(id);
	}

	/* annotations */
	@DELETE
	@Path("/messages/{id}")
	public void removeMessage(@PathParam("id") Long id) {
		System.out.println("efface le message d'id = " + id);
		MessageList.getInstance().delMessage(id);
	}

}
