package mri.socket.tcp.echo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServeurTCP {
	public static void main(String[] args) {
		try (ServerSocket server = new ServerSocket(9999)) {
			boolean running = true;
			System.out.println("Server up and running");
			while (running) {
				Socket client = server.accept();
				System.out.println("New client");
				traiterSocketCliente(client);
				System.out.println("Done...");
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void traiterSocketCliente(Socket socketVersUnClient) {
		PrintWriter writer = null;
		BufferedReader reader = null;
		try {
			writer = creerPrinter(socketVersUnClient);
			reader = creerReader(socketVersUnClient);

			String msg = recevoirMessage(reader);
			while (msg != null) {
				System.out.println("got " + msg);
				envoyerMessage(writer, msg);
				msg = recevoirMessage(reader);
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null)
				writer.close();
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	public static BufferedReader creerReader(Socket socketVersUnClient)
			throws IOException {
		return new BufferedReader(new InputStreamReader(
				socketVersUnClient.getInputStream()));
	}

	public static PrintWriter creerPrinter(Socket socketVersUnClient)
			throws IOException {
		return new PrintWriter(socketVersUnClient.getOutputStream());
	}

	public static String recevoirMessage(BufferedReader reader)
			throws IOException {
		return reader.readLine();
	}

	public static void envoyerMessage(PrintWriter printer, String message)
			throws IOException {
		printer.println(message);
		printer.flush();
	}
}