package mri.socket.tcp.charset;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.logging.Logger;

public class ClientTCP {
	private static final Logger log = Logger.getLogger(ClientTCP.class.getSimpleName());
	
	public static void main(String[] args) {
		if (args.length < 2) {
			System.err.println("Missing args");
			System.exit(-1);
		}
		PrintWriter printer = null;
		BufferedReader reader = null;
		try (Socket client = new Socket("localhost", 9999)) {
			printer = creerPrinter(client, args[1]);
			reader = creerReader(client, args[1]);
			envoyerNom(printer, args[0]);
			String input = "";
			boolean running = true;
			while (running) {
				input = lireMessageAuClavier();
				if (!input.equals("fin")) {
					envoyerMessage(printer, input);
					System.out.println(recevoirMessage(reader));
				} else {
					running = false;
				}
			}
			System.out.println("ended...");
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (printer != null)
				printer.close();
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	// Lire un message au clavier
	// envoyer le message au serveur
	// recevoir et afficher la réponse du serveur
	public static String lireMessageAuClavier() throws IOException {
		String input = null;
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		input = in.readLine();
		return input;
	}

	public static BufferedReader creerReader(Socket socketVersUnClient,
			String charset) throws IOException {
		return new BufferedReader(new InputStreamReader(
				socketVersUnClient.getInputStream(), charset));
	}

	public static PrintWriter creerPrinter(Socket socketVersUnClient,
			String charset) throws IOException {
		return new PrintWriter(new OutputStreamWriter(
				socketVersUnClient.getOutputStream(), charset));
	}

	public static String recevoirMessage(BufferedReader reader)
			throws IOException {
		return reader.readLine();
	}

	public static void envoyerMessage(PrintWriter printer, String message)
			throws IOException {
		printer.println(message);
		printer.flush();
	}

	public static void envoyerNom(PrintWriter printer, String nom)
			throws IOException {
		envoyerMessage(printer, "Name: " + nom);
	}

}
