package mri.socket.tcp.chat;

import java.net.Socket;

public class TraiteUnClient implements Runnable {
	private Socket socketVersUnClient;
	private String charset;

	public TraiteUnClient(Socket socket, String charset) {
		this.socketVersUnClient = socket;
		this.charset = charset;
	}

	@Override
	public void run() {
		ServeurTCP.traiterSocketCliente(socketVersUnClient, charset);
	}

}
