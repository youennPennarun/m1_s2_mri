package mri.socket.address;

import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.List;

public class AfficheInterfaces {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			Enumeration<NetworkInterface> interfaces = NetworkInterface
					.getNetworkInterfaces();
			while (interfaces.hasMoreElements()) {
				NetworkInterface i = interfaces.nextElement();
				List<InterfaceAddress> adresses = i.getInterfaceAddresses();
				System.out.println(i.getName() + ": " + i.getDisplayName());
				for (InterfaceAddress a : adresses) {
					System.out.println("=> " + a.getAddress());
				}
				System.out.println();
			}
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

}
