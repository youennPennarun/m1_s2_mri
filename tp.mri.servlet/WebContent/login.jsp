<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="./css/styles.css" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><%= request.getAttribute("title") %></title>
</head>
<body>
	<div class="header">
		<img class="header-img" src="./assets/gent.jpg" width="100px" height="100px"/>
		<p class="title">Login page</h1>
	</div>
	<form name='loginForm' action='chat' method='post'>
		<label>Username: </label>
		<input type='text' name='username' value=''>
		<input type='submit' name='action' value='Login'/>
	</form>
</body>
</html>