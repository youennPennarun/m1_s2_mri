<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" href="./css/styles.css" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><%= request.getAttribute("title") %></title>
</head>
<body>
	<div class="header">
		<img class="header-img" src="./assets/gent.jpg" width="100px" height="100px"/>
		<p class="title">Gentleman chat</h1>
	</div>
	<p class="welcome"><%= pageContext.getServletContext().getInitParameter("welcome") %></p>
	<form name='chatForm' action='chat' method='post'>
		<input type='text' name='ligne' value=''>
		<input type='submit' name='action' value='Submit'/>
		<input type='submit' name='action' value='Refresh'/>
		<input type='submit' name='action' value='Logout'/>
	</form>
	<%= request.getAttribute("content") %>
</body>
</html>