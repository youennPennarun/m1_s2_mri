
import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class ChatServlet
 */
@WebServlet("/chat")
public class ChatServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String BASE_TITLE = "Gentleman Chat";
	private StringBuffer chatContent;
	private String welcomeMsg;

	/**
	 * Default constructor.
	 */
	public ChatServlet() {}

	public void init() throws ServletException {
		chatContent = new StringBuffer();
		welcomeMsg = getServletContext().getInitParameter("welcome");
		/*
		 * chatContent = new StringBuffer(); chatContent .append(
		 * "Bienvenue sur le chat") .append("\n") .append("Soyez polis")
		 * .append("\n");
		 * 
		 * form = new StringBuffer(); form.append(
		 * "<form name='chatForm' action='chat' method='post'>") .append(
		 * "\t<input type='text' name='ligne' value=''>") .append(
		 * "\t<input type='submit' name='action' value='submit'/>") .append(
		 * "\t<input type='submit' name='action' value='refresh'/>")
		 * .append("</form>");
		 */

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = getUsername(request);
		
		if (username == null) { // User isn't logged in
			response.setContentType("text/html");
			request.setAttribute("title", BASE_TITLE + " - " + "login");
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.include(request, response);
		} else {// User is logged in
			response.setContentType("text/html");
			request.setAttribute("title", BASE_TITLE);
			RequestDispatcher rd = request.getRequestDispatcher("chat.jsp");
			request.setAttribute("content", chatContent.toString());
			request.setAttribute("welcome", welcomeMsg);
			rd.include(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("action");
		System.out.println(action);
		switch (action) {
		case "Submit":
			String nl = request.getParameter("ligne");
			if (nl != null && !nl.equals("")) {
				chatContent.append(new Date() + "  <span class='username'>" + getUsername(request)
						+ "</span> > <span class='message'>" + nl + "</span><br>");
			}
			break;
		case "Login":
			String username = request.getParameter("username");
			setUsername(request, username);
			break;
		case "Logout":
			setUsername(request, null);
			break;
		default:
			break;
		}
		//doGet(request, response);
        response.sendRedirect("chat");
	}

	private void setUsername(HttpServletRequest request, String username) {
		boolean create = true;
		/* Get request session, or create it if it doesn't exists */
		HttpSession session = request.getSession(create);
		/* Set value for key "user" */
		session.setAttribute("user", username);
	}

	private String getUsername(HttpServletRequest request) {
		boolean create = true;
		/* Get request session, or create it if it doesn't exists */
		HttpSession session = request.getSession(create);
		/* Get and return value for key "user" */
		return (String) session.getAttribute("user");
	}

}
