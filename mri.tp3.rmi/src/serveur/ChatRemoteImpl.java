package serveur;

import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;

import common.ChatRemote;
import common.ReceiveCallback;

public class ChatRemoteImpl extends UnicastRemoteObject implements ChatRemote {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2639255274104532123L;

	private ArrayList<ReceiveCallback> callbacks = new ArrayList<>();

	protected ChatRemoteImpl() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String echo(String name, String message) throws RemoteException {
		return name + ">" + message;
	}

	@Override
	public void send(String name, String message) throws RemoteException {
		Iterator<ReceiveCallback> callbacksIterator = callbacks.iterator();
		while (callbacksIterator.hasNext()) {
			ReceiveCallback callback = callbacksIterator.next();
			try {
				callback.newMessage(echo(name, message));
			} catch (ConnectException e) {
				System.out.println("callback removed");
				callbacksIterator.remove();
			}
		}
	}

	@Override
	public void registerCallback(ReceiveCallback callback)
			throws RemoteException {
		callbacks.add(callback);
	}

}
