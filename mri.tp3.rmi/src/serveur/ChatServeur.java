package serveur;

import java.net.MalformedURLException;
import java.nio.file.Paths;
import java.rmi.AlreadyBoundException;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class ChatServeur {
	private final static String BASE_URL = "//localhost/";

	public static void main(String[] args) throws MalformedURLException,
			RemoteException, AlreadyBoundException {
		String pathToClasses = Paths.get("bin").toUri().toURL().toString();
		System.setProperty("java.rmi.server.codebase", pathToClasses);

		ChatRemoteImpl chatRemote = new ChatRemoteImpl();
		Naming.rebind(BASE_URL + "chat", chatRemote);
	}
}
