package serveur;

import java.rmi.ConnectException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;

import common.ChatRemoteBonus;
import common.ReceiveCallback;

public class ChatRemoteImplBonus extends UnicastRemoteObject implements
		ChatRemoteBonus {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2639255274104532123L;

	private ArrayList<Room> rooms = new ArrayList<Room>();

	// private ArrayList<ReceiveCallback> callbacks = new ArrayList<>();

	protected ChatRemoteImplBonus() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String echo(String name, String message) throws RemoteException {
		return name + ">" + message;
	}

	@Override
	public void send(String room, String name, String message)
			throws RemoteException {
		boolean found = false;
		for (Room r : rooms) {
			if (r.getName().equals(room)) {
				r.broadcast(name, message);
				found = false;
				break;
			}
		}

	}

	@Override
	public void registerCallback(ReceiveCallback callback)
			throws RemoteException {

	}

	@Override
	public void join(String room, ReceiveCallback callback) {
		// TODO Auto-generated method stub

	}

	@Override
	public void leave(String room, ReceiveCallback callback) {
		// TODO Auto-generated method stub

	}

}

class Room {
	private String roomName;
	private ArrayList<ReceiveCallback> callbacks = new ArrayList<ReceiveCallback>();

	Room(String roomName) {
		this.roomName = roomName;
	}

	public String getName() {
		return roomName;
	}

	public boolean hasJoined(ReceiveCallback callback) {
		return callbacks.contains(callback);
	}

	public void registerCallback(ReceiveCallback callback)
			throws RemoteException {
		callbacks.add(callback);

	}

	public void broadcast(String name, String message) throws RemoteException {
		Iterator<ReceiveCallback> callbacksIterator = callbacks.iterator();
		while (callbacksIterator.hasNext()) {
			ReceiveCallback callback = callbacksIterator.next();
			try {
				callback.newMessage(name + ">" + message);
			} catch (ConnectException e) {
				System.out.println("callback removed");
				callbacksIterator.remove();
			}
		}
	}
}
