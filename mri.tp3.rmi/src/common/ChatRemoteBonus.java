package common;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ChatRemoteBonus extends Remote {
	public String echo(String name, String message) throws RemoteException;

	public void send(String room, String name, String message)
			throws RemoteException;

	public void registerCallback(ReceiveCallback callback)
			throws RemoteException;

	public void join(String room, ReceiveCallback callback)
			throws RemoteException;

	public void leave(String room, ReceiveCallback callback)
			throws RemoteException;
}
