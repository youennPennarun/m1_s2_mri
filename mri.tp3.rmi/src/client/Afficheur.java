package client;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import common.ReceiveCallback;

public class Afficheur extends UnicastRemoteObject implements ReceiveCallback {

	protected Afficheur() throws RemoteException {
		super();
	}

	@Override
	public void newMessage(String message) throws RemoteException {
		System.out.println(message);
	}

}
