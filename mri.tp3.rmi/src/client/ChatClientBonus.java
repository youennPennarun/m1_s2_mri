package client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

import common.ChatRemoteBonus;

public class ChatClientBonus {

	public ChatClientBonus(ChatRemoteBonus chat) throws RemoteException {
		Afficheur a = new Afficheur();
		chat.registerCallback(a);
		readInputs(chat, a);
	}

	public static void main(String[] args) throws MalformedURLException,
			RemoteException, NotBoundException {
		String ip;
		if (args.length < 1) {
			System.err.println("Missing ip, Usage: client.jar <ip>");
			// System.exit(-1);
			System.out.println("Using localhost");
			ip = "localhost";
		} else {
			ip = args[0];
		}
		ChatRemoteBonus chat = (ChatRemoteBonus) Naming.lookup("//" + ip
				+ "/chat");
		new ChatClientBonus(chat);

	}

	private void readInputs(final ChatRemoteBonus chat, final Afficheur a) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Scanner in = new Scanner(System.in);
				System.out.print("Name: ");
				String name = in.nextLine();
				while (true) {
					System.out.print(">");
					String s = in.nextLine();
					String[] splitted = s.split(":");
					if (splitted.length == 2) {
						if (splitted[0].equals("join")) {
							try {
								chat.join(splitted[1], a);
							} catch (RemoteException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else if (splitted[0].equals("leave")) {
							try {
								chat.leave(splitted[1], a);
							} catch (RemoteException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						} else {
							System.err
									.println("unknown command " + splitted[0]);
						}
					} else {
						try {
							chat.send(name, s, s);
						} catch (RemoteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}).start();

	}
}
