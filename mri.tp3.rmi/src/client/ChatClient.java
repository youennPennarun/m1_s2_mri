package client;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

import common.ChatRemote;

public class ChatClient {

	public ChatClient(ChatRemote chat) {

		try {
			chat.registerCallback(new Afficheur());
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		readInputs(chat);
	}

	public static void main(String[] args) throws MalformedURLException,
			RemoteException, NotBoundException {
		String ip;
		if (args.length < 1) {
			System.err.println("Missing ip, Usage: client.jar <ip>");
			// System.exit(-1);
			System.out.println("Using localhost");
			ip = "localhost";
		} else {
			ip = args[0];
		}
		ChatRemote chat = (ChatRemote) Naming.lookup("//" + ip + "/chat");
		new ChatClient(chat);

	}

	private void readInputs(final ChatRemote chat) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Scanner in = new Scanner(System.in);
				System.out.print("Name: ");
				String name = in.nextLine();

				while (true) {
					System.out.print(">");
					String s = in.nextLine();
					try {
						chat.send(name, s);
					} catch (RemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();

	}
}
