package src.chat;

import javax.ws.rs.core.MediaType;

import com.sun.jersey.api.client.WebResource;

public class Afficheur implements Runnable {
	private long lastId = 0;

	private WebResource service;

	public Afficheur(WebResource service) {
		this.service = service;
		Message[] messages = service.path("messages/").accept(MediaType.APPLICATION_JSON).get(Message[].class);
		handleResponse(messages);

	}

	public void run() {
		Message[] messages = service.path("messages/after/" + (lastId + 1)).accept(MediaType.APPLICATION_JSON)
				.get(Message[].class);
		handleResponse(messages);
	}

	private void handleResponse(Message[] messages) {
		for (Message m : messages) {
			if (m.getId() > lastId) {
				lastId = m.getId();
			}
			System.out.println(m);
		}
	}
}
