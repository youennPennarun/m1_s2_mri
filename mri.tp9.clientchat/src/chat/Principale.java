package src.chat;

import java.net.URI;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class Principale {

	private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

	public static void main(String[] args) {
		Principale p = new Principale();
		p.readInputs();
	}

	private void readInputs() {

		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);

		URI serviceURI = UriBuilder.fromUri("http://localhost:8080/mri.jersey/rest").build();
		WebResource service = client.resource(serviceURI);

		Afficheur afficheur = new Afficheur(service);
		scheduler.scheduleAtFixedRate(afficheur, 1, 1, TimeUnit.SECONDS);

		Scanner in = new Scanner(System.in);

		while (true) {
			System.out.print(">");
			String s = in.nextLine();
			Message m = new Message();
			m.setContent(s);

			m = (Message) service.path("messages").accept(MediaType.APPLICATION_JSON).post(Message.class, m);

		}
	}

}
